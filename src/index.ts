const outCanvas = document.getElementById("cv") as HTMLCanvasElement;
const outCtx = outCanvas.getContext("2d") as CanvasRenderingContext2D;
const canvas = document.createElement("canvas");
const context = canvas.getContext("2d") as CanvasRenderingContext2D;

canvas.width = 500;
canvas.height = 500;

const inputTxt = document.getElementById("txt-input");
const inputCRMAX = document.getElementById("crmax-input");

const valuesList = document.getElementById("values-list") as HTMLUListElement;
const padding = 50;

const colors = [

    "#e51c23",
    "#e91e63",
    "#9c27b0",
    "#673ab7",
    "#3f51b5",
    "#5677fc",
    "#03a9f4"

];

function clearCanvas(cv: HTMLCanvasElement) {
    cv.width = cv.width;
}


function plotGraph() {

    clearCanvas(canvas);
    clearCanvas(outCanvas);

    // plot ordinate scale
    // plot abciss scale
    unit.plotScale(context, curve.curveList[0].values.length - 1);
    // plot curves
    curve.curveList.forEach(element => {
        element.plot(context);
    });

    outCanvas.width = canvas.width;
    outCanvas.height = canvas.height;

    outCtx.drawImage(canvas, 0, 0);

}



function importTxt(result: String) {
    curve.init();
    unit.init();

    let lines = result.split("\r\n");
    let lineCursor = 0;
    for (; lineCursor < lines.length && !lines[lineCursor].startsWith("Total Frames:"); ++lineCursor)
        continue; // skip the strart

    let nbFrames = 0;
    let nbItems = 0;
    { // gather some metadata
        let parameters = lines[lineCursor].split(",");
        nbFrames = parseInt(parameters[0].split(":")[1].trim(), 10);
        nbItems = parseInt(parameters[1].split(":")[1].trim(), 10);
    }
    if (nbFrames <= 0 || nbItems <= 0) //error
        return -1;

    lineCursor += 2; // should go to the first entry
    for (let i = 0; i < nbItems; ++i) {
        let line = lines[lineCursor + i + 1].split("-----");

        let itemName = line[0].trim();

        let charCursor = line[1].length - 1;
        while (charCursor > 0) {
            if (isNaN(parseInt(line[1][charCursor])))
                --charCursor;
            else
                break;
        }
        let itemUnit = line[1].slice(charCursor + 1);
        curve.curveList.push(new curve.Curve(itemUnit, itemName));

    }
    let entries = result.split("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\r\n");
    for (let i = 1; i < entries.length; ++i) {
        let entryLines = entries[i].split("\r\n");
        for (let j = 1; j < entryLines.length; ++j) {
            let line = entryLines[j];
            if (line.length > 0)
                curve.curveList[j - 1].addValue(parseFloat(line.split("-----")[1].trim()));
        }
    }

    plotGraph();
}




function openFile(el: HTMLInputElement, e: Event, callback: (str: string) => any) {
    if (el.files) {
        if (el.files.length > 0) {
            let file = el.files[0];
            if (file) {
                let reader = new FileReader();
                reader.addEventListener("load", (e) => {
                    if (e.target?.result) {
                        let result = e.target.result.toString();
                        callback(result);
                    }
                });
                reader.readAsText(file);
            }
        }
    }
}

inputTxt?.addEventListener("change", function (this: HTMLInputElement, e: Event) { openFile(this, e, importTxt); });


outCanvas.addEventListener("mousemove", (ev) => {
    clearCanvas(outCanvas);
    outCtx.drawImage(canvas, 0, 0);


    let x = ev.offsetX;
    // let y = ev.offsetY
    if (curve.curveList.length > 0) {
        let nbFrames = curve.curveList[0].values.length - 1;
        let n = 0;
        if (x > canvas.width - padding)
            n = nbFrames;
        if (x >= padding && x <= canvas.width - padding) {
            let fraction = (x - padding) / (canvas.width - padding * 2);
            n = Math.round(fraction * nbFrames);
        }
        curve.curveList.forEach((c) => {
            c.showValue(n, outCtx);
        });
        let horizontalScale = (outCanvas.width - padding * 2) / nbFrames;
        outCtx.clearRect(n * horizontalScale + padding, outCanvas.height - padding + 2, n.toString().length * 10 + 7, 20);
        outCtx.save();
        outCtx.beginPath();
        outCtx.strokeStyle = "black";
        outCtx.lineWidth = 2;
        outCtx.moveTo(n * horizontalScale + padding, outCanvas.height - padding + 5);
        outCtx.lineTo(n * horizontalScale + padding, outCanvas.height - padding - 5);
        outCtx.stroke();
        outCtx.font = "15px Arial";
        outCtx.fillText(n.toString(), n * horizontalScale + padding + 3, outCanvas.height - padding + 13);
        outCtx.closePath();
        outCtx.restore();
    }

});