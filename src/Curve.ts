module curve {

    export function init() { // used to init an reset 
        curveList = []
        availableColors = colors.slice()
        valuesList.innerHTML = ""
    }

    export let curveList = [] as Array<Curve>


    const colors = [

        "#e51c23",
        "#e91e63",
        "#9c27b0",
        "#673ab7",
        "#3f51b5",
        "#5677fc",
        "#03a9f4"

    ]
    let availableColors = [] as Array<string>


    export class Curve {
        unit: unit.Unit
        values: Array<number>
        description: string
        color: string
        lineWidth = 2
        showing = true
        minValue: undefined | number = undefined
        maxValue: undefined | number = undefined
        span: HTMLSpanElement
        constructor(unitTxt: string, description: string, values: Array<number> = [], color: string = "") {
            this.unit = unit.getUnitFromTxt(unitTxt)
            this.values = values
            this.description = description
            if (color.length > 0)
                this.color = color
            else {
                if (availableColors.length == 0)
                    this.color = "black"
                else {
                    let r = Math.floor(Math.random() * (availableColors.length - 1))
                    this.color = availableColors.splice(r, 1)[0]
                }
            }
            this.span = document.createElement("span")
            this.addButton()
        }
        private addButton() {
            let li = document.createElement("li")
            let label = document.createElement("label")
            label.style.color = this.color
            let input = document.createElement("input")
            input.type = "checkbox"
            input.checked = this.showing
            input.addEventListener("change", (e) => {
                if (input.checked) this.showing = true
                else this.showing = false
                plotGraph()
            })
            this.updateSpan()
            label.appendChild(input)
            label.appendChild(this.span)
            li.appendChild(label)
            valuesList.appendChild(li)
        }
        showValue(n: number, ctx: CanvasRenderingContext2D) {
            if (this.showing) {

                let horizontalScale = this.getHorizontalScale(ctx)
                let x = horizontalScale * n + padding
                let value = this.values[n]
                let y = this.unit.computeOrdinate(value, ctx)

                ctx.save()
                ctx.beginPath()
                ctx.fillStyle = "white"
                ctx.strokeStyle = "black"
                ctx.arc(x, y, 5, 0, Math.PI * 2)

                ctx.fill()
                ctx.stroke()
                ctx.closePath()
                ctx.fillStyle = "black"
                ctx.font = "20px Arial"
                ctx.fillText(value.toString(), x, y - 5)
                ctx.restore()
            }

        }

        updateSpan() {
            let range = ""
            if (this.minValue != this.maxValue && this.minValue != NaN)
                range = `${this.minValue} - ${this.maxValue}`
            if (this.minValue == this.maxValue && this.minValue != undefined)
                range = `${this.minValue}`
            this.span.innerText = `${this.description} : ${range} ${this.unit.str}`

        }

        addValue(x: number) {
            if (this.minValue == undefined)
                this.minValue = x
            if (this.maxValue == undefined)
                this.maxValue = x

            if (this.minValue > x)
                this.minValue = x
            if (this.maxValue < x)
                this.maxValue = x

            this.values.push(x)
            this.unit.addValue(x)

        }

        private getHorizontalScale(ctx: CanvasRenderingContext2D) {
            return (ctx.canvas.width - padding * 2) / (this.values.length - 1)

        }
        plot(ctx: CanvasRenderingContext2D) {
            this.updateSpan()
            if (this.showing) {
                let horizontalScale = this.getHorizontalScale(ctx)
                ctx.beginPath()
                ctx.strokeStyle = this.color
                ctx.lineWidth = this.lineWidth
                ctx.moveTo(padding, this.unit.computeOrdinate(this.values[0], ctx))
                for (let i = 1; i < this.values.length; ++i) {
                    ctx.lineTo(i * horizontalScale + padding, this.unit.computeOrdinate(this.values[i], ctx))
                }
                ctx.stroke()
                ctx.closePath()
            }

        }
    }

}