module unit {

    export function init() {
        units = []
    }

    const defaultMin = -2
    const defaultMax = +2

    export let units = [] as Array<Unit>


    export class Unit {
        str: string
        maxValue: number
        minValue: number

        constructor(txt: string) {
            this.str = txt
            this.maxValue = defaultMax
            this.minValue = defaultMin
        }

        getScale(ctx: CanvasRenderingContext2D) {
            return (ctx.canvas.height - padding * 2) / (this.maxValue - this.minValue)
        }
        computeOrdinate(x: number, ctx: CanvasRenderingContext2D) {
            // console.log(this.str, this.getScale(ctx), x, (x + this.minValue) * this.getScale(ctx) + padding)
            return ctx.canvas.height - ((x - this.minValue) * this.getScale(ctx) + padding)
        }
        addValue(x: number) {
            if (x > this.maxValue) {
                this.maxValue = x
            }
            if (x < this.minValue) {
                this.minValue = x
            }
        }
    }

    export function getUnitFromTxt(txt: string) {
        let index = -1
        units.forEach((v, i) => {
            if (v.str === txt) {
                index = i
                return
            }
        })
        if (index != -1) {
            return units[index]
        }
        // if nothing was found 
        else {

            let u = new Unit(txt)
            units.push(u)
            return u
        }

    }

    export function plotScale(ctx: CanvasRenderingContext2D, nbFrames: number) {
        ctx.beginPath()
        ctx.moveTo(padding, ctx.canvas.height - padding)
        ctx.lineTo(ctx.canvas.width - padding, ctx.canvas.height - padding)
        // context.transform(1, 0, 0, -1, 0, ctx.canvas.height)
        ctx.fillText("Frames", padding, ctx.canvas.height - padding + 30)
        // context.transform(1, 0, 0, -1, 0, 0)
        const horizontalScale = (ctx.canvas.width - padding * 2) / (nbFrames)

        for (let i = 0; i <= nbFrames; ++i) {
            if (i % 10 == 0) {

                ctx.moveTo(i * horizontalScale + padding, ctx.canvas.height - padding - 5)
                ctx.lineTo(i * horizontalScale + padding, ctx.canvas.height - padding + 5)
                ctx.fillText((i).toString(), i * horizontalScale + padding, ctx.canvas.height - padding + 15)
            }
        }
        ctx.stroke()
        ctx.closePath()

        if (units.length > 0) {
            ctx.beginPath()
            ctx.moveTo(padding, padding)
            ctx.lineTo(padding, ctx.canvas.height - padding)
            ctx.fillText(units[0].str, 0, padding - 10)


            ctx.moveTo(padding - 5, padding)
            ctx.lineTo(padding + 5, padding)
            ctx.fillText(units[0].maxValue.toString(), 0, padding + 12)

            ctx.moveTo(padding - 5, ctx.canvas.height - padding)
            ctx.lineTo(padding + 5, ctx.canvas.height - padding)
            ctx.fillText(units[0].minValue.toString(), 0, ctx.canvas.height - padding - 2)

            ctx.stroke()
            ctx.closePath()
        }
        if (units.length > 1) {
            ctx.beginPath()
            ctx.moveTo(ctx.canvas.width - padding, padding)
            ctx.lineTo(ctx.canvas.width - padding, ctx.canvas.height - padding)

            ctx.moveTo(ctx.canvas.width - padding - 5, padding)
            ctx.lineTo(ctx.canvas.width - padding + 5, padding)
            ctx.fillText(units[1].maxValue.toString(), ctx.canvas.width - padding + 4, padding + 12)


            ctx.moveTo(ctx.canvas.width - padding - 5, ctx.canvas.height - padding)
            ctx.lineTo(ctx.canvas.width - padding + 5, ctx.canvas.height - padding)
            ctx.fillText(units[1].minValue.toString(), ctx.canvas.width - padding + 4, ctx.canvas.height - padding - 2)




            ctx.fillText(units[1].str, ctx.canvas.width - padding, padding - 10)


            ctx.stroke()
            ctx.closePath()
        }
    }

}